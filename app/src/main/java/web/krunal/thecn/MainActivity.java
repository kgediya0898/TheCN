package web.krunal.thecn;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.webkit.DownloadListener;
import android.webkit.JsPromptResult;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
private int requestCode=1;
    private  Context context;
    private WebView webview;
private ProgressBar pb;
    private ImageView imgV;
    private TextView txtV;

    private ValueCallback<Uri[]> mUploadMessage;
    private final static int FILECHOOSER_RESULTCODE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_PROGRESS);

        setContentView(R.layout.activity_main);
        getWindow().setFeatureInt( Window.FEATURE_PROGRESS, Window.PROGRESS_VISIBILITY_ON);
        //Getting Imageviews And WebViews
imgV=(ImageView)findViewById(R.id.noInternet);

         webview = (WebView)findViewById(R.id.webview);
         //Setting Visibility to INVISIBLE so that it remains invisible until it loads
        webview.setVisibility(View.INVISIBLE);
        //Setting caching properties for performance enchancements
        
        webview.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        
        //Getting Prograss Bar And keep it visible until webview loads
        pb = (ProgressBar)findViewById(R.id.progressBar);
        //NO INTERNET Textview getter
        
txtV=(TextView)findViewById(R.id.txtV);

//Enabling JavaScript Actions in Webview
webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setDomStorageEnabled(true);

//Setting Download Listener to Download Attachments from posts and elsewhere
webview.setDownloadListener(new DownloadListener() {
    @Override
    public void onDownloadStart(String s, String s1, String s2, String s3, long l) {

//Splitting URL of file to get File Name
        String temp[]=s.split("/");
        
        //Checking For Storage Permission 
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
        DownloadManager.Request request =new DownloadManager.Request(Uri.parse(s));
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,temp[temp.length-1]);
        DownloadManager dm= (DownloadManager)getSystemService(DOWNLOAD_SERVICE);
        dm.enqueue(request);

//If succesful Make Toast Message
        Toast.makeText(getApplicationContext(),"Downloading CN Attachment",Toast.LENGTH_SHORT).show();
        }else {
            
//Else requestPermission and make toast for the same
            Toast.makeText(getApplicationContext(), "Can't Download The File! Permission Missing", Toast.LENGTH_SHORT).show();
        }
    }
});
//getting activity context
final Activity activity = this;

//Setting Webview Client for JS actions and progress
webview.setWebChromeClient(new WebChromeClient() {
    @Override
    public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, JsPromptResult result) {

        return super.onJsPrompt(view, url, message, defaultValue, result);

    }
//Opening file manager to upload files for CN attachments
    @Override
    public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
        openFileChooser(filePathCallback);
        return true;
    }

    public void onProgressChanged(WebView view, int progress) {
    activity.setProgress(progress * 1000);
}
});
webview.setWebViewClient(new WebViewClient() {
//When page is loaded Request Storage permissions and make Progress Bar INVISIBLE and Show Hidden Webview
    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        pb.setVisibility(View.GONE);
        webview.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

              ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestCode);
            }
        }

    }

    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
Toast.makeText(activity, "Oh no! " + description, Toast.LENGTH_SHORT).show();
}
});

//Check for internet Availability
checkInternet();



    }
    //If user presses back goto previous page if it is available else exit
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (webview.canGoBack()) {
                        webview.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }

//Get connectivity Status
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;

    }
    public void checkInternet(){
        if(isNetworkConnected()){
            txtV.setVisibility(View.GONE);
            imgV.setVisibility(View.GONE);
            webview.loadUrl("https://www.thecn.com/");

        }else{

        }
    }
    public void openFileChooser(ValueCallback<Uri[]> uploadMsg) {

        mUploadMessage = uploadMsg;
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.addCategory(Intent.CATEGORY_OPENABLE);
        i.setType("*/*");
        startActivityForResult(Intent.createChooser(i, "File Chooser"), FILECHOOSER_RESULTCODE);
    }
//Upon choosing file return passed file back to Webview Activity
    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {
        if(resultCode!=0){
        if (requestCode == FILECHOOSER_RESULTCODE) {
            Uri result = intent == null || resultCode != RESULT_OK ? null
                    : intent.getData();
            Uri[] resultsArray = new Uri[1];
            resultsArray[0] = result;
            mUploadMessage.onReceiveValue(resultsArray);
            mUploadMessage=null;
        }
    }}
//Check Internet status on pressing On TAP TO RETRY Text
public void reload(View view){
  checkInternet();
}}